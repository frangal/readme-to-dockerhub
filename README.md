README to Docker Hub
===

This is a quick hack to push README.md files to Docker hub.


# How to use?

Simply build the image using `docker build -t readme-dockerhub .`

and run it with all needed parameter:

```console
docker run --rm \
    -v /path/to/readme.md:/data/README.md \
    -e DOCKERHUB_USERNAME=myhubuser \
    -e DOCKERHUB_PASSWORD=myhubpassword \
    -e DOCKERHUB_REPO_PREFIX=myorga \
    -e DOCKERHUB_REPO_NAME=myrepo \
     readme-dockerhub
```

That's it.


## Environment variables

This image uses environment variables for configuration.

|Available variables     |Default value        |Description                                         |
|------------------------|---------------------|----------------------------------------------------|
|`DOCKERHUB_USERNAME`    |no default           |The Username (not mail address) used to authenticate|
|`DOCKERHUB_PASSWORD`    |no default           |Password of the `DOCKERHUB_USERNAME`-user           |
|`DOCKERHUB_REPO_PREFIX` |`$DOCKERHUB_USERNAME`|Organization or username for the repository         |
|`DOCKERHUB_REPO_NAME`   |no default           |Name of the repository you want to push to          |
|`README_PATH`           |`/data/README.md`    |Path to the README.me to push                       |
|`SHORT_DESCRIPTION`     |no default           |Short description for the Dockerhub repo            |

## Mount the README.md

By default, if the `README_PATH` environment variable is not set, this image always pushes the file
`/data/README.md` as full description to Docker Hub.

For GitHub repositories you can use `-v /path/to/repository:/data/`.

If your description is not named `README.md` mount the file directory using `-v /path/to/description.md:/data/README.md`.

*Notice that the filename is case sensitive. If your readme is called `readme.md` you have to mount the file directly, not the directory*


# License

View [license information](https://www.npmjs.com/package/docker-hub-api) for the software contained in this image.

Everything in [this repository](https://github.com/SISheogorath/readme-to-dockerhub) is published under [GPL-3](https://spdx.org/licenses/GPL-3.0).
