FROM alpine:latest

ARG NODE_VERSION
ARG NODE_URL

RUN addgroup -g 1000 node \
    && adduser -u 1000 -G node -s /bin/sh -D node \
    && apk add --no-cache libstdc++ curl \
    && curl -fsSLO --compressed "$NODE_URL/$NODE_VERSION/node-$NODE_VERSION-linux-x64-musl.tar.xz" \
    && tar -xJf "node-$NODE_VERSION-linux-x64-musl.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
    && ln -s /usr/local/bin/node /usr/local/bin/nodejs \
    && rm -Rf "node-$NODE_VERSION" \
    && rm -f "node-$NODE_VERSION-linux-x64-musl.tar.xz" \
    # smoke tests
    && node --version \
    && npm --version \
    && apk del curl

COPY index.js package*.json /app/

WORKDIR /app/

RUN npm --no-update-notifier --silent install

ENTRYPOINT ["node", "--unhandled-rejections=strict", "/app/index.js"]

# Metadata params
ARG BUILD_DATE
ARG VERSION
ARG VCS_URL
ARG VCS_REF
ARG DESCRIPTION

# https://github.com/opencontainers/image-spec/blob/master/annotations.md
LABEL org.opencontainers.image.created="$BUILD_DATE" \
      org.opencontainers.image.source="$VCS_URL" \
      org.opencontainers.image.revision="$VCS_REF" \
      org.opencontainers.image.vendor='FrangaL' \
      org.opencontainers.image.version="$VERSION" \
      org.opencontainers.image.title="README to Docker Hub" \
      org.opencontainers.image.description="$DESCRIPTION" \
      org.opencontainers.image.url='https://hub.docker.com/repository/docker/frangal/readme-dockerhub' \
      org.opencontainers.image.authors="@sheogorath <sheogorath@shivering-isles.com>"
